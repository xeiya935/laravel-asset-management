@extends('layouts.app')
@section('title', "View Transactions")
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="text-center">Transactions</h1>
				<div class="col-lg-4 offset-lg-4">
					<form action="/transactionindex" method="POST" class="d-flex">
						@csrf
						<input type="text" name="search" class="form-control" placeholder="Search Transactions By Transaction Code">
						<button type="submit" class="btn btn-secondary ml-1">
							<i class="fas fa-search"></i>
						</button>
					</form>
					@if(Session::has("search"))
						<h4 class="my-3 text-danger">{{Session::get('search')}}</h4>
					@elseif(Session::has("deleteTransaction"))
						<h3 class="my-3 text-danger">{{Session::get('deleteTransaction')}}</h3>
					@endif
				</div>
				<div class="col-lg-3">
					<form action="/transactionindex" method="POST" class="d-flex">
						@csrf
						<select class="form-control" name="transaction_filter">
							<option value="0">All Transactions</option>
							@foreach($statuses as $status)
					    		<option value="{{ $status->id }}">
					    			{{ $status->name }}
					    		</option>
					    	@endforeach
						</select>
						<button type="submit" class="btn btn-warning mr-1">
							<i class="fas fa-filter"></i>
						</button>
					</form>
				</div>
				<table class="table table-striped my-3 text-center">
					<thead>
						<th>Transaction Code</th>
						<th>Requestor</th>
						<th>Asset Name</th>
						<th>Items Requested</th>
						<th>Borrow Date</th>
						<th>Return Date</th>
						<th>Status</th>
						<th>Actions</th>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
						<tr>
							<td class="align-middle">{{ $transaction->transaction_code }}</td>
							<td class="align-middle">{{ $transaction->user->name }}</td>
							<td class="align-middle">{{ $transaction->asset->name }}</td>
							<td class="align-middle">{{ $transaction->quantity }}</td>
							<td class="align-middle">{{ $transaction->borrow_date }}</td>
							<td class="align-middle">{{ $transaction->return_date}}</td>
							<td class="align-middle">
								{{ $transaction->rqst_status }}
								<form action="/transaction/{{ $transaction->id }}" method="POST">
									@csrf
									@method('PATCH')
									<input type="hidden" name="transaction_id" value="{{ $transaction->id }}">
									<select class="form-control" name="status_id">
										@foreach($statuses as $status)
											<option value="{{ $status->id }}">{{ $status->name }}</option>
										@endforeach
									</select>
									<button type="submit" class="btn btn-info my-2"
										@if($transaction->rqst_status == 5 || $transaction->reqst_status == 2)
											disabled
										@endif
									>
										Update
									</button>
								</form>
							</td>
							<td class="align-middle">
								<form action="/transaction/{{ $transaction->id }}" method="POST">
									@csrf
									@method('PATCH')
									<input type="hidden" name="status_id" value="5">
									<button class="btn btn-success"
										@if($transaction->rqst_status == 5)
											disabled
										@elseif($transaction->rqst_status == 1)
											hidden
										@endif
									>
										Return Asset
									</button>
								</form>
								@if($transaction->rqst_status == 1)
									<a href="/transaction/{{ $transaction->id }}/edit" class="btn btn-info my-2">Edit</a>
								@endif
								<form action="/transaction/{{ $transaction->id }}" method="POST" class="my-2">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger"
										@if($transaction->rqst_status == 5 || $transaction->rqst_status == 2)
											disabled
										@endif
									>
										Delete
									</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection