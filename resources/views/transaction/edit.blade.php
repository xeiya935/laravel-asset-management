@extends('layouts.app')
@section('title', 'Edit Transaction')
@section('content')
	<div class="container py-5">
		<div class="row">
			<div class="col-lg-12">
				<h1>Request for {{ $transaction->asset->name }}</h1>
				<p>Transaction Code: {{ $transaction->transaction_code }}</p>
				<p>Requestor: {{ $transaction->user->name }}</p>
				<p>Available stocks: {{ $transaction->asset->stock }} </p>
				<div class="col-lg-4 px-0">
					<form action="/transaction/{{ $transaction->id }}" method="POST">
						@csrf
						@method('PATCH')
						<input type="hidden" name="transaction_id" value="{{ $transaction->id }}">
						<div class="form-group">
							<label for="quantity">Quantity</label>
							<input type="number" name="quantity" class="form-control" placeholder="Quantity" value="{{ $transaction->quantity }}" min="1" max="{{ $transaction->asset->stock }}">
							@error('quantity')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<div class="form-group">
							<label for="borrow_date">Borrow Date</label>
							<input type="date" name="borrow_date" class="form-control" value="{{ $transaction->borrow_date }}">
							@error('borrow_date')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<div class="form-group">
							<label for="return_date">Return Date</label>
							<input type="date" name="return_date" class="form-control" value="{{ $transaction->return_date }}">
							@error('return_date')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<button type="submit" class="btn btn-primary ml-1">
							Edit Transaction
						</button>
					</form>
				</div>
				@if(Session::has("editTransaction"))
					<h4 class="my-3 text-success">{{Session::get('editTransaction')}}</h4>
				@endif
			</div>
		</div>
	</div>
@endsection