@extends('layouts.app')
@section('title', 'Assets')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>Assets</h1>
				<div class="col-lg-4 offset-lg-4">
					<form action="/assetindex" method="POST" class="d-flex">
						@csrf
						<input type="text" name="search" class="form-control" placeholder="Search Assets">
						<button type="submit" class="btn btn-secondary ml-1">
							<i class="fas fa-search"></i>
						</button>
					</form>
					@if(Session::has("search"))
						<h4 class="my-3 text-danger">{{Session::get('search')}}</h4>
					@endif
				</div>
				<div class="d-flex justify-content-between">
					<div class="col-lg-3">
						<form action="/assetindex" method="POST" class="d-flex">
							@csrf
							<select class="form-control" name="category_filter">
								<option value="0">All Categories</option>
								<option value="a-z">A-Z</option>
								<option value="z-a">Z-A</option>
								@foreach($categories as $category)
						    		<option value="{{ $category->id }}">
						    			{{ $category->name }}
						    		</option>
						    	@endforeach
							</select>
							<button type="submit" class="btn btn-warning mr-1">
								<i class="fas fa-filter"></i>
							</button>
						</form>
					</div>
					<a href="/asset/create" class="btn btn-success">Add Asset</a>
				</div>

				@if(Session::has("addAsset"))
					<h3 class="my-3 text-danger">{{Session::get('addAsset')}}</h3>
				@elseif(Session::has("deleteAsset"))
					<h3 class="my-3 text-danger">{{Session::get('deleteAsset')}}</h3>
				@elseif(Session::has("editAsset"))
					<h3 class="my-3 text-danger">{{Session::get('editAsset')}}</h3>
				@endif
				<table class="table table-striped my-3">
					<thead>
						<th>Image</th>
						<th>Asset Code</th>
						<th>Asset</th>
						<th>Stocks Available</th>
						<th>Category</th>
						<th>Description</th>
						<th>Actions</th>
					</thead>
					<tbody>
						@foreach($assets as $asset)
						<tr>
							<td>
								<img src="{{ URL::asset($asset->image) }}" class="img-thumbnail w-50 h-75">
							</td>
							<td class="align-middle">{{ $asset->code }}</td>
							<td class="align-middle">{{ $asset->name }}</td>
							<td class="align-middle">{{ $asset->stock }}</td>
							<td class="align-middle">{{ $asset->category->name }}</td>
							<td class="align-middle">{{ $asset->description }}</td>
							<td class="align-middle">
								<a href="/asset/{{ $asset->id }}" class="btn btn-success d-block">Request Asset</a>
								<a href="/asset/{{ $asset->id }}/edit" class="btn btn-info my-2">Edit</a>
								<form action="/asset/{{ $asset->id }}" method="POST" class="my-2">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection