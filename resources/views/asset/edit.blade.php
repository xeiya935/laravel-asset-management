@extends('layouts.app')
@section('title', 'Edit Asset')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<h1 class="text-center">Edit Asset</h1>
				<form action="/asset/{{ $asset->id }}" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PATCH')
					<div class="form-group">
					    <label for="name">Asset Name</label>
					    <input type="text" class="form-control" name="name" value="@if(isset($asset)){{ $asset->name }}@else{{ old('name') }}@endif">
					    @error('name')
					    	<small class="text-danger">Cannot leave input blank.</small>
					    @enderror
					</div>
					<div class="form-group">
					    <label for="stock">Available Stock</label>
					    <input type="text" class="form-control" name="stock" value="@if(isset($asset)){{ $asset->stock }}@else{{ old('stock') }}@endif">
					    @error('stock')
					    	<small class="text-danger">Cannot leave input blank.</small>
					    @enderror
					</div>
					<div class="form-group">
					    <label for="category_id">Category</label>
					    <select class="form-control" name="category_id">
					    	<option>Select a category</option>
					    	@foreach($categories as $category)
					    		<option value="{{ $category->id }}"
					    			@if($asset->category_id == $category->id)
					    				selected
					    			@endif
					    		>
					    			{{ $category->name }}
					    		</option>
					    	@endforeach
					    </select>
				    	@error('category')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					</div>
					<div class="form-group">
						<label for="description">Description</label>
					    <textarea class="form-control" name="description">@if(isset($asset)){{ $asset->description }}@else{{ old('description') }}@endif</textarea>
					    @error('description')
					    	<small class="text-danger">Cannot leave input blank.</small>
					    @enderror
					</div>
					<div class="form-group">
						<label for="image">Asset Image</label>
						<input type="file" name="image" class="form-control-file">
						@error('image')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					    <h3 class="text-center my-3">Current Image</h3>
					    <div class="d-flex justify-content-center">
							<img src="{{ URL::asset($asset->image) }}" class="img-thumbnail">
						</div>
					</div>
					<div class="text-center">
						
					</div>
					<button type="submit" class="btn btn-info">Edit Asset</button>
				</form>
			</div>
		</div>
	</div>
@endsection