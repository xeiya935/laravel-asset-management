@extends('layouts.app')
@section('title', 'Add Asset')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<h1 class="text-center">Add Asset</h1>
				<form action="/asset" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
					    <label for="name">Asset Name</label>
					    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
					    @error('name')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					</div>
					<div class="form-group">
					    <label for="stock">Available Stock</label>
					    <input type="text" class="form-control" name="stock" value="{{ old('stock') }}">
					    @error('stock')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					</div>
					<div class="form-group">
					    <label for="category_id">Category</label>
					    <select class="form-control" name="category_id">
					    	@foreach($categories as $category)
					    		<option value="{{ $category->id }}">
					    			{{ $category->name }}
					    		</option>
					    	@endforeach
					    </select>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
					    <textarea class="form-control" name="description">{{ old('description') }}</textarea>
					    @error('description')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					</div>
					<div class="form-group">
						<label for="image">Asset Image</label>
						<input type="file" name="image" class="form-control-file">
						@error('image')
					    	<small class="text-danger">{{ $message }}</small>
					    @enderror
					</div>
					<button type="submit" class="btn btn-success">Add Asset</button>
				</form>
			</div>
		</div>
	</div>
@endsection