@extends('layouts.app')
@section('title', "$asset->name")
@section('content')
	<div class="container py-5">
		<div class="row">
			<div class="col-lg-4">
				<img src="{{ URL::asset($asset->image) }}" class="img-thumbnail">
			</div>
			<div class="col-lg-8">
				<h1>{{ $asset->name }}</h1>
				<p>Asset Code: {{ $asset->code }}</p>
				<p>Category: {{ $asset->category->name }}</p>
				<p>Description: {{ $asset->description }}</p>
				<p>Number of stocks available: {{ $asset->stock }}</p>
				<div class="col-lg-4 px-0">
					<form action="/transaction" method="POST">
						@csrf
						<input type="hidden" name="asset_id" value="{{ $asset->id }}">
						<div class="form-group">
							<label for="quantity">Quantity</label>
							<input type="number" name="quantity" class="form-control" placeholder="Quantity" value="1" min="1" max="{{ $asset->stock }}">
							@error('quantity')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<div class="form-group">
							<label for="borrow_date">Borrow Date</label>
							<input type="date" name="borrow_date" class="form-control">
							@error('borrow_date')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<div class="form-group">
							<label for="return_date">Return Date</label>
							<input type="date" name="return_date" class="form-control">
							@error('return_date')
						    	<small class="text-danger">{{ $message }}</small>
						    @enderror
						</div>
						<button type="submit" class="btn btn-success ml-1">
							Request
						</button>
					</form>
				</div>
				@if(Session::has("addTransaction"))
					<h4 class="my-3 text-success">{{Session::get('addTransaction')}}</h4>
				@endif
			</div>
		</div>
	</div>
@endsection