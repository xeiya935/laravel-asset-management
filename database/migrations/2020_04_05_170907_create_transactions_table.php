<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_code')->default(strtotime("now"));
            $table->unsignedBigInteger('user_id')
            ->on('users')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->unsignedBigInteger('asset_id')
            ->on('assets')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->unsignedBigInteger('quantity');
            $table->date('borrow_date');
            $table->date('return_date');
            $table->string('rqst_status')->default(1)
            ->on('statuses')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
