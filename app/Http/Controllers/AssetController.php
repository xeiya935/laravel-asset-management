<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Category;
use Session;

class AssetController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();

        if(isset($request->search)){
            $assets = Asset::query()->where('name', 'LIKE', "%$request->search%")->get();
        } else {
            $category_filter = $request->category_filter;
            if($category_filter == "a-z") {
                $assets = Asset::all()->sortBy('name');
            } elseif($category_filter == "z-a") {
                $assets = Asset::all()->sortByDesc('name');
            } elseif($category_filter > 0){
                $assets = Asset::all()->where('category_id', $category_filter);
            } else {
                $assets = Asset::all();
            }
        }

        if(count($assets) == 0){
            Session::flash("search", "No asset found");
        } else {
            Session::flash("search", "");
        }

        return view('asset.index', compact('assets', 'categories'));
    }

    public function create()
    {
    	$categories = Category::all();
    	return view('asset.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => 'required',
            'stock' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'image' => "required|image|mimes:jpeg,png,gif,jpg,svg"
        ]);

        $data['name'] = ucwords($data['name']);

        if($request->file('image')!=null){
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination,$image_name);
            $data['image'] = $destination.$image_name;
            $asset = \App\Asset::create($data);
        }

        $addAssetFlash = ucwords($request->name);
        Session::flash("addAsset", "Item successfully added: $addAssetFlash");
        return redirect('asset');
    }

    public function destroy(Asset $asset){

        $asset->delete();

        Session::flash("deleteAsset", "Item successfully removed: $asset->name");
        return redirect('asset');
    }

    public function show($id){
        $asset = Asset::find($id);
        return view('asset.show', compact('asset'));
    }

    public function edit($id){
        $asset = Asset::find($id);
        $categories = Category::all();

        return view('asset.edit', compact('asset', 'categories'));
    }

    public function update($id, Request $request){

        $data = request()->validate([
            'name' => 'required',
            'stock' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'image' => "image|mimes:jpeg,png,gif,jpg,svg"
        ]);

        $assetToEdit = Asset::find($id);

        $assetToEdit->name = $request->name;
        $assetToEdit->stock = $request->stock;
        $assetToEdit->category_id = $request->category_id;
        $assetToEdit->description = $request->description;

        if($request->file('image')!=null){
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination,$image_name);
            $assetToEdit->image = $destination.$image_name;
        }

        $assetToEdit->save();

        Session::flash("editAsset", "Item successfully updated: $assetToEdit->name");
        return redirect('asset');
    }
}
