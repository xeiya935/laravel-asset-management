<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Asset;
use App\Status;
use Session;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statuses = Status::all();

        if(isset($request->search)){
            $transactions = Transaction::query()->where('transaction_code', "$request->search")->first()->get();
            dd($transactions);
        } else {
            $transaction_filter = $request->transaction_filter;
            
            if($transaction_filter > 0){
                $transactions = Transaction::all()->where('rqst_status', $transaction_filter);
            } else {
                $transactions = Transaction::all();
            }
        }

        if(is_null($transactions)){
            Session::flash("search", "No transaction found");
        } else {
            Session::flash("search", "");
        }

        // $statuses = Status::all();
        // $transactions = Transaction::all();
        return view('transaction.index', compact('transactions', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'quantity' => 'required',
            'borrow_date' => 'required',
            'return_date' => 'required',
        ]);

        $data['user_id'] = Auth::user()->id;
        $data['asset_id'] = $request->asset_id;

        // dd($data);

        $transaction = \App\Transaction::create($data);

        $asset = Asset::find($request->asset_id);

        Session::flash("addTransaction", "A request has been sent for $request->quantity $asset->name.");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $statuses = Status::all();
        // dd($transaction);

        return view('transaction.edit', compact('transaction', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transactionToEdit = Transaction::find($id);

        if(isset($request->status_id)){
            $asset = Asset::find($transactionToEdit->asset_id);
            if($request->status_id == 2){
                $asset->stock -= $transactionToEdit->quantity;
                $asset->save();
            } elseif($request->status_id == 5){
                $asset->stock += $transactionToEdit->quantity;
                $asset->save();
            }
            $transactionToEdit->rqst_status = $request->status_id;
            
        } else {
            $data = request()->validate([
            'quantity' => 'required',
            'borrow_date' => 'required',
            'return_date' => 'required',
            ]);

            $transactionToEdit->quantity = $request->quantity;
            $transactionToEdit->borrow_date = $request->borrow_date;
            $transactionToEdit->return_date = $request->return_date;

            Session::flash("editTransaction", "Item successfully updated: $transactionToEdit->asset->name. Transaction Code: transactionToEdit->transaction_code");
        }
        
        $transactionToEdit->save();
        return redirect('transaction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $asset = Asset::find($transaction->asset_id);

        $transaction->delete();

        Session::flash("deleteTransaction", "Transaction successfully removed: $asset->name. Transaction Code: $transaction->transaction_code");
        return redirect('transaction');
    }
}
