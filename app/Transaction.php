<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $guarded = [];
	
    public function user(){
    	return $this->belongsTo(User::class); 
	}

	public function asset(){
		return $this->belongsTo(Asset::class);
	}

	public function status(){
		return $this->belongsTo(Status::class);
	}
}