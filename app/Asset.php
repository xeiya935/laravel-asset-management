<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
	protected $guarded = [];
	
    public function category(){
    	return $this->belongsTo(Category::class); 
	}

	public function transactions(){
    	return $this->hasMany(Transaction::class); 
	}
}
