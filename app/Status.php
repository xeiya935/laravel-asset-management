<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
	protected $guarded = [];
	
	public function transactions(){
        return $this->hasMany(Transaction::class);
    }
}
