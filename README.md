# Laravel-Asset-Management

Laravel training - capstone 2

This app is an Asset Management application that was created and would work for a category of assets mainly dealing with borrowing and returning assets. The application mainly focuses on a user requesting for assets and the administrator approving or rejecting the request and ensuring that all the assets are properly accounted for.

The examples provided in this project is for an Electronics and Appliance asset management application. This was made using Laravel 6 for the frontend and using mySQL as the database for the backend.

The application works with the following features that allows interaction between users and administrators:

Asset functionality
	1) Admin can add assets
		- Assets can be added that contain the following:
			i) Asset code
			ii) Asset Name
			iii) Stocks
			iv) Category
			v) Asset description
			vi) Asset Image 
	2) Admin and users can view all the assets
		- Both users will be redirected to the view assets page where they are able to do different actions such as create a request and add, edit and delete an asset. 
	3) Admin can update the information about the assets
	4) Admin can delete assets
	5) Admin and users can search and view assets for individual assets
		- All users can search and view all assets related to the search using either a portion or the full name of the asset
	6) Admin and users can filter assets and view all assets by category
		- All users can filter the assets:
			i) Alphabetically based on the asset name
			ii) By category
		
Request/Transaction functionality
	1) Users can request for assets from the entity
		- After requesting the request is sent for admin approval
	2) Users and admins can view all transactions related to them
	3) Users can edit their requests and Admins can edit the user’s request
		- Users can edit the quantity, borrow and return dates for the items
		- Admin can edit the status of the requests as well as edit the fields users can edit
	4) Admin can delete user’s requests
Transaction history
	1) Users and administrators can view the individual transaction history for individual assets - Feature to be added
	2) Admin can approve and reject the requests
		- Once the administrator approves the request, the assets will be deducted from the total number of stocks available
		- Administrators can also set the status of a request to pending, cancelled and returned for easier and accurate asset tracking
	3) Admin can filter and search through all requests  - Search feature to be added
	4) User returns items back to the entity
		- Once the user or the administrator returns the assets, the amount of the stocks available will increase
Other functionalities
	1) User login/registration
	2) Separation of concerns - Feature to be added
	3) Authorization/authentication on all pages - Feature to be added
	4) Error checking

Additional notes:
	1) Resource controllers were used for the routes
	2) Gates and policies were used for authentication and separation of concerns - Function to be added

